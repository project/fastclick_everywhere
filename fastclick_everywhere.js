if(!window.addEventListener) {
  window.attachEvent('load', function() {
    FastClick.attach(document.body);
  });
}
else {
  window.addEventListener('load', function() {
    FastClick.attach(document.body);
  }, false);
}
