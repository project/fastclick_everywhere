FastClick Everywhere is a simple module than adds the FastClick library
to every page. This means that the fastclick behaviour will be in effect
in all pages of your Drupal website. 

If you need the old click behaviour in some elements, add to them the
class 'needsclick'. See https://github.com/ftlabs/fastclick for more
details and instructions.

Credits
-------
This Drupal module is maintained by Pere Orga <pere@orga.cat>.
